Acknowledging or Citing aiapy
=============================

If you use aiapy in your scientific work, we would appreciate you citing it in your publications.

Please add the following line within your methods, conclusion or acknowledgements sections:

   *This research used version X.Y.Z (software citation) of the aiapy open source
   software package.*

The software citation should be the specific `Zenodo DOI`_ for the version used within your work.

.. code:: bibtex

    @software{barnes_w_t_2020_4016983,
      author = {Barnes, W. T. and Cheung, M. C. M and Padmanabhan, N. and Chintzoglou, G. and Mumford, S. and Wright, P. J. and Shih, A. Y. and Bobra, M. G. and Shirman, N. and Kocher, M.},
      title        = {aiapy},
      month        = jul,
      year         = 2020,
      publisher    = {Zenodo},
      version      = {v0.2.0},
      doi          = {10.5281/zenodo.4016983},
      url          = {https://doi.org/10.5281/zenodo.4016983}
      }

You can also obtain this information with ``aiapy.__citation__``.

.. _Zenodo DOI: https://zenodo.org/record/4016983
