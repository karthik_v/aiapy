Getting Started
================

The recommended way to install aiapy is with `pip`,

.. code-block:: shell

   pip install aiapy

You can also install the development version from GitLab,

.. code-block:: shell

   git clone https://gitlab.com/LMSAL_HUB/aia_hub/aiapy.git
   cd aiapy
   pip install -e .

If you will be developing aiapy, please see the :ref:`dev-guide`.
