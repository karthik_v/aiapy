aiapy
======

aiapy is a Python package for analyzing data from the Atmospheric Imaging
Assembly (AIA) instrument onboard the Solar Dynamics Observatory spacecraft.

aiapy includes software for converting AIA images from level 1 to level 1.5,
point spread function deconvolution, and computing the wavelength and temperature
response functions for the EUV channels.


.. toctree::
  :maxdepth: 2

  getting_started
  generated/gallery/index
  code_ref/index
  develop
  whatsnew/index
  about
